module Translator (translateLangF, translateLangL) where

import Control.Monad.Free.Church (foldF)
import Control.Monad.Reader (Reader, ask, local, runReader)
import Control.Monad.Writer (WriterT, execWriterT, tell)
import Data.DList (DList)
import qualified Data.DList as DList (fromList, toList)
import Data.Function ((&))
import Data.Functor (($>))

import Language (LangF (..), LangL)

type TranslateM a = WriterT (DList Char) (Reader Int) a

newtype IDRef a = IDRef Int

refName :: IDRef a -> String
refName (IDRef id') = "v" <> show id'

translateLangF :: Int -> LangF IDRef a -> TranslateM a
translateLangF offset cons = do
  spaces offset
  case cons of
    Int v cont     -> cont v <$ tells (show v)
    Bool v cont    -> cont v <$ tells (if v then "t" else "nil")
    String v cont  -> cont v <$ tells (show v)
    Nil cont       -> cont <$ tells "nil"
    (:=.) a b cont -> cont <$> op2 "eql" (==) a b
    (:>.) a b cont -> cont <$> op2 ">" (>) a b
    (:+.) a b cont -> cont <$> op2 "+" (+) a b
    (:-.) a b cont -> cont <$> op2 "-" (-) a b
    (:*.) a b cont -> cont <$> op2 "*" (*) a b
    Abs' a cont    -> cont . abs <$> op1 "abs" a
    Signum' a cont -> cont . signum <$> op1 "signum" a
    Mod' a b cont  -> cont <$> op2 "mod" mod a b
    If' cond t f cont -> do
      tells "(if"
      condV <- translateLangL' 1 cond
      tells "\n"
      tV <- translateLangL' (offset + 2) t
      tells "\n"
      fV <- translateLangL' (offset + 2) f
      tells ")" $> (cont $ if condV then tV else fV)
    While cond body cont -> do
      tells "(loop while "
      _ <- translateLangL' 0 cond
      tells " do\n"
      _ <- translateLangL' (offset + 2) body
      tells ")" $> cont
    Lambda1 fun cont -> do
      tells "(lambda ("
      ref <- IDRef <$> ask
      tells (refName ref <> ")\n") *> spaces (offset + 2)
      tells "(progn\n"
      _ <- local (+ 1) $
        translateLangL' (offset + 4) (fun ref)
      tells "))" $> cont fun
    Call1 funExpr argExpr cont -> do
      tells "("
      _ <- translateLangL' 0 funExpr
      tells "\n"
      _ <- translateLangL' (offset + 2) argExpr
      tells ")" $> cont stub
    Funcall1 funRef arg cont ->
      cont stub <$ op1 ("funcall " <> refName funRef) arg
    Readf ref cont -> cont stub <$ tells (refName ref)
    Setf ref v cont -> cont <$> op1 ("setf " <> refName ref) v
    Print' v cont -> cont <$ op1 "print" v
  where
    stub = error "don't use binds in free-script"

translateLangL'
  :: Int -> LangL IDRef a -> TranslateM a
translateLangL' offset = foldF (translateLangF offset)

translateLangL :: LangL IDRef a -> String
translateLangL a
  = translateLangL' 0 a
  & execWriterT
  & flip runReader 0
  & DList.toList

tells :: String -> TranslateM ()
tells = tell . DList.fromList

spaces :: Int -> TranslateM ()
spaces offset = tells (replicate offset ' ')

op2 :: String -> (a -> b -> c) -> LangL IDRef a -> LangL IDRef b -> TranslateM c
op2 fTr f a b = do
  tells ("(" <> fTr)
  aV <- translateLangL' 1 a
  bV <- translateLangL' 1 b
  tells ")" $> f aV bV

op1 :: String -> LangL IDRef a -> TranslateM a
op1 fTr a = tells ("(" <> fTr) *> translateLangL' 1 a <* tells ")"
