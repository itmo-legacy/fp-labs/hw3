module Runner (interpretLangF, runLangL, runFunction) where

import Control.Monad (when)
import Control.Monad.Free.Church (foldF)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)

import Language (LangF (..), LangL)

op2 :: (a -> b -> c) -> LangL IORef a -> LangL IORef b -> (c -> r) -> IO r
op2 f a b cont = cont <$> (f <$> runLangL a <*> runLangL b)

interpretLangF :: LangF IORef a -> IO a
interpretLangF cons = case cons of
  Int v cont     -> pure (cont v)
  Bool v cont    -> pure (cont v)
  String v cont  -> pure (cont v)
  Nil cont       -> pure cont
  (:=.) a b cont -> op2 (==) a b cont
  (:>.) a b cont -> op2 (>) a b cont
  (:+.) a b cont -> op2 (+) a b cont
  (:-.) a b cont -> op2 (-) a b cont
  (:*.) a b cont -> op2 (*) a b cont
  Abs' a cont    -> cont . abs <$> runLangL a
  Signum' a cont -> cont . signum <$> runLangL a
  Mod' a b cont  -> op2 mod a b cont
  If' cond t f cont -> do
    condV <- runLangL cond
    resV <- runLangL (if condV then t else f)
    pure (cont resV)
  While cond body cont -> do
    while (runLangL cond) (runLangL body)
    pure cont
  Lambda1 f cont -> pure (cont f)
  Call1 fE argE cont -> do
    f <- runLangL fE
    arg <- runLangL argE >>= newIORef
    res <- runLangL (f arg)
    pure (cont res)
  Funcall1 fRef argE cont -> do
    f <- readIORef fRef
    arg <- runLangL argE >>= newIORef
    res <- runLangL (f arg)
    pure (cont res)
  Readf ref cont -> cont <$> readIORef ref
  Setf ref v cont -> do
    vV <- runLangL v
    writeIORef ref vV
    cont <$> readIORef ref
  Print' a cont -> do
    aV <- runLangL a
    print aV
    pure cont

runLangL :: LangL IORef a -> IO a
runLangL = foldF interpretLangF

runFunction :: (IORef a -> LangL IORef b) -> a -> IO b
runFunction f x = do
  xRef <- newIORef x
  runLangL (f xRef)

while :: Monad m => m Bool -> m a -> m ()
while cond body = do
  condV <- cond
  when condV (body *> while cond body)
