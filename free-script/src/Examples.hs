{-# LANGUAGE TypeApplications #-}

module Examples
  ( log2
  , fizzbuzz
  , refLambdas
  )
  where

import Language

log2 :: LangL r (r Integer -> LangL r Integer)
log2 =
  (lambda1 $ \n ->
    (let1 (int 1) $ \accum -> let1 (int 0) $ \logCount ->
      (do
        (while ((>.) (readf n) (readf accum))
          (do
            (void (setf accum ((*) (readf accum) (int 2))))
            (incf logCount)))
        (readf logCount))))

fizzbuzz :: LangL r (r Integer -> LangL r ())
fizzbuzz =
  (lambda1 $ \x ->
    (if' ((=.) (int 0) (mod' (readf x) (int 15)))
      (print' (string "FizzBuzz"))
      (if' ((=.) (int 0) (mod' (readf x) (int 3)))
        (print' (string "Fizz"))
        (if' ((=.) (int 0) (mod' (readf x) (int 5)))
          (print' (string "Buzz"))
          (print' (readf x))))))

refLambdas :: LangL r ()
refLambdas =
  (let1 (lambda1 $ \x -> (incf x)) $ \inc ->
    (do
      (print' (funcall1 inc (int 5)))
      (print' (funcall1 inc (int 8)))))
