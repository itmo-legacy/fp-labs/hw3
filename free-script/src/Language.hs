{-# LANGUAGE DeriveFunctor        #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE StandaloneDeriving   #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Language
  ( LangF (..)
  , LangL

  , int
  , bool
  , string
  , nil

  , (=.)
  , (>.)

  , mod'

  , if'
  , while
  , lambda1
  , call1
  , funcall1
  , readf
  , setf
  , print'

  , incf
  , let1
  , when

  , void
  ) where

import Prelude hiding (Bool, String)
import qualified Prelude (Bool, String)

import Control.Monad (void)
import Control.Monad.Free.Church (F (..), MonadFree, liftF)
import Control.Monad.Free.TH (makeFree_)

data LangF r next where
  Int :: Integer -> (Integer -> next) -> LangF r next
  Bool :: Prelude.Bool -> (Prelude.Bool -> next) -> LangF r next
  String :: Prelude.String -> (Prelude.String -> next) -> LangF r next
  Nil :: next -> LangF r next

  (:=.) :: Eq a => LangL r a -> LangL r a -> (Prelude.Bool -> next) -> LangF r next
  (:>.) :: Ord a => LangL r a -> LangL r a -> (Prelude.Bool -> next) -> LangF r next

  (:+.) :: Num a => LangL r a -> LangL r a -> (a -> next) -> LangF r next
  (:-.) :: Num a => LangL r a -> LangL r a -> (a -> next) -> LangF r next
  (:*.) :: Num a => LangL r a -> LangL r a -> (a -> next) -> LangF r next
  Abs' :: Num a => LangL r a -> (a -> next) -> LangF r next
  Signum' :: Num a => LangL r a -> (a -> next) -> LangF r next

  Mod' :: LangL r Integer -> LangL r Integer -> (Integer -> next) -> LangF r next

  If' :: LangL r Prelude.Bool -> LangL r a -> LangL r a -> (a -> next) -> LangF r next
  While :: LangL r Prelude.Bool -> LangL r a -> next -> LangF r next
  Lambda1 :: (r a -> LangL r b) -> ((r a -> LangL r b) -> next) -> LangF r next
  Call1 :: LangL r (r a -> LangL r b) -> LangL r a -> (b -> next) -> LangF r next
  Funcall1 :: r (r a -> LangL r b) -> LangL r a -> (b -> next) -> LangF r next
  Readf :: r a -> (a -> next) -> LangF r next
  Setf :: r a -> LangL r a -> (a -> next) -> LangF r next
  Print' :: Show a => LangL r a -> next -> LangF r next
deriving instance Functor (LangF r)

type LangL r = F (LangF r)

makeFree_ ''LangF

int :: Integer -> LangL r Integer
bool :: Prelude.Bool -> LangL r Prelude.Bool
string :: Prelude.String -> LangL r Prelude.String
nil :: LangL r ()

(=.) :: Eq a => LangL r a -> LangL r a -> LangL r Prelude.Bool
(>.) :: Ord a => LangL r a -> LangL r a -> LangL r Prelude.Bool

(+.) :: LangL r Integer -> LangL r Integer -> F (LangF r) Integer
(-.) :: LangL r Integer -> LangL r Integer -> F (LangF r) Integer
(*.) :: LangL r Integer -> LangL r Integer -> F (LangF r) Integer
abs' :: LangL r Integer -> LangL r Integer
signum' :: LangL r Integer -> LangL r Integer

mod' :: LangL r Integer -> LangL r Integer -> LangL r Integer

if' :: LangL r Prelude.Bool -> LangL r a -> LangL r a -> LangL r a
while :: LangL r Prelude.Bool -> LangL r a -> LangL r ()
lambda1 :: (r a -> LangL r b) -> LangL r (r a -> LangL r b)
call1 :: LangL r (r a -> LangL r b) -> LangL r a -> LangL r b
funcall1 :: r (r a -> LangL r b) -> LangL r a -> LangL r b
readf :: r a -> LangL r a
setf :: r a -> LangL r a -> LangL r a
print' :: Show a => LangL r a -> LangL r ()

instance Num (LangL r Integer) where
  (+) = (+.)
  (-) = (-.)
  (*) = (*.)
  abs = abs'
  signum = signum'
  fromInteger = int

incf :: r Integer -> LangL r Integer
incf ref = (setf ref ((+.) (readf ref) (int 1)))

let1 :: LangL r a -> (r a -> LangL r b) -> LangL r b
let1 x f = (call1 (lambda1 f) x)

when :: LangL r Prelude.Bool -> LangL r () -> LangL r ()
when cond t = if' cond t nil
