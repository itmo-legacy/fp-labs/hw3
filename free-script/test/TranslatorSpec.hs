module TranslatorSpec (spec) where

import Data.List (intercalate)

import Test.Hspec (Spec, describe, it, shouldBe)

import Examples (fizzbuzz, log2, refLambdas)
import Translator (translateLangL)

spec :: Spec
spec = do
  describe "translateLangL" $ do
    it "correctly translates 'Examples.log2' function" $ do
      translateLangL log2 `shouldBe` intercalate "\n"
        [ "(lambda (v0)"
        ,"  (progn"
        ,"    ((lambda (v1)"
        ,"  (progn"
        ,"    ((lambda (v2)"
        ,"  (progn"
        ,"    (loop while (> v0 v1) do"
        ,"      (setf v1 (* v1 2))      (setf v2 (+ v2 1)))    v2))"
        ,"      0)))"
        ,"      1)))"
        ]

    it "correctly translates 'Examples.fizzbuzz' function" $ do
      translateLangL fizzbuzz `shouldBe` intercalate "\n"
        [ "(lambda (v0)"
        , "  (progn"
        , "    (if (eql 0 (mod v0 15))"
        , "      (print \"FizzBuzz\")"
        , "      (if (eql 0 (mod v0 3))"
        , "        (print \"Fizz\")"
        , "        (if (eql 0 (mod v0 5))"
        , "          (print \"Buzz\")"
        , "          (print v0))))))"
        ]

    it "correctly translates 'Examples.refLambdas' function" $ do
      translateLangL refLambdas `shouldBe` intercalate "\n"
        [ "((lambda (v0)"
        , "  (progn"
        , "    (print (funcall v0 5))    (print (funcall v0 8))))"
        , "  (lambda (v0)"
        , "    (progn"
        , "      (setf v0 (+ v0 1)))))"
        ]
