{-# LANGUAGE NamedFieldPuns  #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Comonad19
  ( oneCarrierGrid
  , evolve
  , printGrid
  , simulate

  , Config
  , defaultConfig
  , setProb
  , setIncub
  , setIll
  , setImmune
  ) where

import Control.Comonad (extend, extract)
import Lens.Micro ((%~), (&), (.~))
import Lens.Micro.TH (makeLenses)
import System.Random (StdGen, mkStdGen, randomR, split)

import Grid (Grid (..), focus, neighbours, slice)
import ListZipper (mkZipper)

data Config = Config
  { cfgProb   :: Double
  , cfgIncub  :: Int
  , cfgIll    :: Int
  , cfgImmune :: Int
  } deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgProb = 0.2
  , cfgIncub = 2
  , cfgIll = 5
  , cfgImmune = 7
  }

setProb :: Double -> Config -> Either String Config
setProb p cfg
  | 0 < p && p < 1 = pure cfg{ cfgProb = p }
  | otherwise = Left "probability should be in the (0, 1) interval"

setIncub :: Int -> Config -> Either String Config
setIncub n cfg
  | n > 0 = pure cfg{ cfgIncub = n }
  | otherwise = Left "incubation period should be positive"

setIll :: Int -> Config -> Either String Config
setIll n cfg
  | n > 0 = pure cfg{ cfgIll = n }
  | otherwise = Left "illness period should be positive"

setImmune :: Int -> Config -> Either String Config
setImmune n cfg
  | n > 0 = pure cfg{ cfgImmune = n }
  | otherwise = Left "immunity period should be positive"

data Cell = Cell
  { _clState :: CellState
  , _clRand  :: StdGen
  } deriving Show

data CellState = Healthy | Incub Int | Ill Int | Immune Int deriving Show

makeLenses ''Cell

type SimGrid = Grid Cell

mkGrid :: Cell -> SimGrid
mkGrid = Grid . mkZipper (fmap nextCellL) (fmap nextCellR) . mkRow
  where
    mkRow = mkZipper nextCellL nextCellR
    nextCellL = clRand %~ (fst . split)
    nextCellR = clRand %~ (snd . split)

oneCarrierGrid :: Int -> SimGrid
oneCarrierGrid seed = healthyGrid & focus . clState .~ Incub 0
  where
    healthyGrid = mkGrid Cell{ _clState = Healthy, _clRand = mkStdGen seed }

evolve ::  Config -> SimGrid -> SimGrid
evolve cfg = extend (infectionRule cfg)

infectionRule :: Config -> SimGrid -> Cell
infectionRule Config{ .. } g = case _clState x of
  Healthy        -> tryInfect
  Incub counter  -> advance Incub counter (Ill 0) cfgIncub
  Ill counter    -> advance Ill counter (Immune 0) cfgIll
  Immune counter -> advance Immune counter Healthy cfgImmune

  where
    x = extract g

    tryInfect =
      let (dice, newGen) = randomR (0, 1) (_clRand x)
      in if dice < (1 - cfgProb)^infectedCo
         then Cell{ _clState = Healthy, _clRand = newGen }
         else Cell{ _clState = Incub 0, _clRand = newGen }

    advance oldState counter newState limit
      | counter + 1 == limit = x{ _clState = newState }
      | otherwise = x{ _clState = oldState (counter + 1) }

    infectedCo = length $ filter (isInfected . extract) $ neighbours g

    isInfected Cell{ _clState } = case _clState of
      Incub _ -> True
      Ill _   -> True
      _       -> False

printGrid :: Int -> SimGrid -> String
printGrid n = unlines . slice n . fmap printCell
  where
    printCell cell = case _clState cell of
      Healthy  -> '_'
      Incub _  -> 'i'
      Ill _    -> '#'
      Immune _ -> '@'

simulate :: Int -> [SimGrid]
simulate seed = iterate (evolve defaultConfig) (oneCarrierGrid seed)
