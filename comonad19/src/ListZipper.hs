{-# LANGUAGE DeriveFunctor #-}

module ListZipper (ListZipper (..), left, right, mkZipper, focus, slice) where

import Control.Comonad (Comonad (..))
import Lens.Micro (Lens', (^.))

data ListZipper a = LZ [a] a [a]
  deriving Functor

instance Comonad ListZipper where
  extract = (^. focus)
  duplicate = mkZipper left right

left :: ListZipper a -> ListZipper a
left (LZ (m' : l) m r) = LZ l m' (m : r)
left _                 = error "left: finite zipper"

right :: ListZipper a -> ListZipper a
right (LZ l m (m' : r)) = LZ (m : l) m' r
right _                 = error "right: finite zipper"

mkZipper :: (a -> a) -> (a -> a) -> a -> ListZipper a
mkZipper leftF rightF x
  = LZ (tail $ iterate leftF x) x (tail $ iterate rightF x)

focus :: Lens' (ListZipper a) a
focus f (LZ l old r) = (\new -> LZ l new r) <$> f old

slice :: Int -> ListZipper a -> [a]
slice n (LZ l m r) = reverse (take n l) <> (m : take n r)
