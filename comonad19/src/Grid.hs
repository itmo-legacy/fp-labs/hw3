{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Grid (Grid (..), focus, neighbours, slice) where

import Control.Applicative (liftA2)
import Control.Comonad (Comonad (..))
import Lens.Micro (Lens')

import ListZipper (ListZipper, mkZipper)
import qualified ListZipper as LZ (focus, left, right, slice)

newtype Grid a = Grid{ unGrid :: ListZipper (ListZipper a) }
  deriving Functor

instance Comonad Grid where
  extract = extract . extract . unGrid
  duplicate = Grid . fmap horizontal . vertical

up :: Grid a -> Grid a
up = Grid . LZ.left . unGrid

down :: Grid a -> Grid a
down = Grid . LZ.right . unGrid

left :: Grid a -> Grid a
left = Grid . fmap LZ.left . unGrid

right :: Grid a -> Grid a
right = Grid . fmap LZ.right . unGrid

horizontal :: Grid a -> ListZipper (Grid a)
horizontal = mkZipper left right

vertical :: Grid a -> ListZipper (Grid a)
vertical = mkZipper up down

focus :: Lens' (Grid a) a
focus f = fmap Grid . LZ.focus (LZ.focus f) . unGrid

neighbours :: Grid a -> [Grid a]
neighbours g
  = map ($ g) $ horizontals <> verticals <> liftA2 (.) horizontals verticals
  where
    horizontals = [left, right]
    verticals = [up, down]

slice :: Int -> Grid a -> [[a]]
slice n = map (LZ.slice n) . LZ.slice n . unGrid
