{-# LANGUAGE TypeApplications #-}

module Main (main) where

import Control.Concurrent.Classy (spawn, takeMVar)
import Control.Monad (replicateM)
import Data.Foldable (for_)

import Criterion.Main (bench, defaultMain, whnfIO)

import CHT (getCHT, newCHT, putCHT)

main :: IO ()
main = defaultMain
  [ bench "put" $ whnfIO runPut
  , bench "get" $ whnfIO runGet
  ]
  where
    runPut = do
      cht <- newCHT
      futures <- replicateM 3 $ spawn $
        for_ [0 .. 1000 :: Int] $ \i -> putCHT i [i] cht
      for_ futures takeMVar

    runGet = do
      cht <- newCHT @IO @Int @Int
      futures <- replicateM 3 $ spawn $ do
        for_ [1 .. 100000 :: Int] $ \i -> getCHT i cht
      for_ futures takeMVar
