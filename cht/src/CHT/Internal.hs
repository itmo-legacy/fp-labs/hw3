{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RecordWildCards           #-}

module CHT.Internal
  ( ConcurrentHashTable(..)
  , Buckets
  , newCHT
  , getCHT
  , putCHT
  , sizeCHT
  , capacity
  , findPlace
  ) where

import Control.Concurrent.Classy (MonadConc, STM, atomically)
import Control.Concurrent.Classy.STM (MonadSTM, TArray, TVar, modifyTVar', newTVar, readTVar,
                                      readTVarConc, writeTVar)
import Control.Monad (when)
import Data.Array.MArray (getBounds, getElems, newArray, readArray, writeArray)
import Data.Hashable (Hashable, hash)
import Data.Maybe (isNothing)
import Data.Traversable (for)

type Buckets stm k v = TArray stm Int [(k, v)]

initCapacity :: Int
initCapacity = 16

loadFactor :: Double
loadFactor = 0.5

-- | A separately chained hash table
data ConcurrentHashTable stm k v = CHT
  { chtBuckets :: TVar stm (Buckets stm k v)
  , chtLoad    :: TVar stm Int
  , chtSize    :: TVar stm Int
  }

-- | Creates a new empty hash table
newCHT :: MonadConc m => m (ConcurrentHashTable (STM m) k v)
newCHT = atomically $ do
  chtBuckets <- newTVar =<< withCapacity initCapacity
  chtSize <- newTVar 0
  chtLoad <- newTVar 0
  pure CHT{ .. }

-- | Retrieves an element from a hash table. Thread-safe
getCHT
  :: (MonadConc m, Eq k, Hashable k)
  => k -> ConcurrentHashTable (STM m) k v -> m (Maybe v)
getCHT k CHT{ .. } = do
  buckets <- readTVarConc chtBuckets
  place <- atomically (findPlace k buckets)
  bucket <- atomically (readArray buckets place)
  pure (lookup k bucket)

-- | Puts an element into a hash table. Thread-safe
putCHT
  :: (MonadConc m, Eq k, Hashable k)
  => k -> v -> ConcurrentHashTable (STM m) k v -> m ()
putCHT k v CHT{ .. } = atomically $ do
  buckets <- readTVar chtBuckets
  InsertRes{ .. } <- insertKV k v buckets
  when irIncSize (modifyTVar' chtSize (+ 1))
  when irIncLoad (modifyTVar' chtLoad (+ 1))
  resizeIfNeeded chtLoad chtBuckets

-- | Retrieves a number of elements in a hash table. Thread-safe, but kinda
-- useless, the information is outdated the moment you receive it.
sizeCHT :: MonadConc m => ConcurrentHashTable (STM m) k v -> m Int
sizeCHT = readTVarConc . chtSize

resizeIfNeeded
  :: (MonadSTM stm, Eq k, Hashable k)
  => TVar stm Int -> TVar stm (Buckets stm k v) -> stm ()
resizeIfNeeded chtLoad chtBuckets = do
  load <- readTVar chtLoad
  buckets <- readTVar chtBuckets
  cap <- capacity buckets

  when (fromIntegral load >= fromIntegral cap * loadFactor) $ do
    newBuckets <- withCapacity (cap * 2)
    elems <- concat <$> getElems buckets
    loadIncreases <- for elems $ \(k, v) -> insertKV k v newBuckets
    let loadIncreases' = map irIncLoad loadIncreases
    let newLoad = length (filter id loadIncreases')

    writeTVar chtBuckets newBuckets
    writeTVar chtLoad newLoad

data InsertRes = InsertRes
  { irIncLoad :: Bool
  , irIncSize :: Bool
  }

insertKV
  :: (MonadSTM stm, Hashable k, Eq k) => k -> v -> Buckets stm k v -> stm InsertRes
insertKV k v buckets = do
  place <- findPlace k buckets
  bucket <- readArray buckets place
  let mChanged = deleteByKey k bucket
  let newBucket = (k, v) : maybe bucket id mChanged
  writeArray buckets place newBucket
  return InsertRes { irIncLoad = null bucket
                   , irIncSize = isNothing mChanged
                   }

-- | Retrieves a number of buckets in a hash table
capacity :: MonadSTM stm => Buckets stm k v -> stm Int
capacity arr = do
  (_, end) <- getBounds arr
  pure (end + 1)

-- | Computes a bucket number for a given key
findPlace :: (MonadSTM stm, Hashable k) => k -> Buckets stm k v -> stm Int
findPlace k buckets = do
  cap <- capacity buckets
  pure (hash k `mod` cap)

withCapacity :: MonadSTM stm => Int -> stm (Buckets stm k v)
withCapacity n = newArray (0, n - 1) []

deleteByKey :: Eq a => a -> [(a, b)] -> Maybe [(a, b)]
deleteByKey k li
  | (l, _ : r) <- span ((/= k) . fst) li = Just (l ++ r)
  | otherwise = Nothing
