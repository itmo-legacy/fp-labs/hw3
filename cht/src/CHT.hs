{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RecordWildCards           #-}

module CHT
  ( ConcurrentHashTable
  , newCHT
  , getCHT
  , putCHT
  , sizeCHT
  ) where

import CHT.Internal
