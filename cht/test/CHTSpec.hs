{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE TypeApplications          #-}

module CHTSpec (spec) where

import Control.Concurrent.Classy (atomically, spawn, takeMVar)
import Control.Concurrent.Classy.STM (readTVar)
import Control.Monad (replicateM, when)
import Control.Monad.Catch (Exception (..), throwM)
import Data.Array.MArray (getBounds, readArray)
import Data.Foldable (for_)
import Data.Traversable (for)

import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.Contrib.HUnit (fromHUnitTest)
import Test.HUnit.DejaFu (testAuto)

import CHT.Internal (ConcurrentHashTable (..), findPlace, getCHT, newCHT, putCHT, sizeCHT)

data CHTError = Inconsistent String
              | forall a. Show a => Incorrect a a

instance Show CHTError where
  show (Inconsistent s)      = "Inconsistent " <> show s
  show (Incorrect expR actR) = "Incorrect " <> show expR <> " " <> show actR

instance Exception CHTError where
  displayException (Inconsistent cause)  = "Inconsitent state: " <> cause
  displayException (Incorrect expR actR)
    = "Incorrect results: \n"
   <> "\texpected: " <> show expR <> "\n"
   <> "\tactual: " <> show actR <> "\n"

spec :: Spec
spec = do
  describe "ConcurrentHashTable" $
    fromHUnitTest $ testAuto consistencyTest

  describe "sizeCHT" $ do
    it "computes size of a hash table" $ do
      cht <- newCHT
      putCHT 'a' (5 :: Int) cht
      putCHT 'b' 8 cht
      putCHT 'a' 9 cht
      size <- sizeCHT cht
      size `shouldBe` 2

  where
    consistencyTest = do
      cht <- newCHT

      futures <- replicateM 3 $ spawn $
        for_ ['a' .. 'l'] $ \ch -> putCHT [ch] (fromEnum ch) cht
      for_ futures takeMVar
      for_ ['a' .. 'l'] $ \ch -> do
        actRes <- getCHT [ch] cht
        let expRes = Just (fromEnum ch)
        when (actRes /= expRes) (throwM $ Incorrect expRes actRes)

      atomically (checkInvariant cht)

    checkInvariant CHT{ .. } = do
      buckets <- readTVar chtBuckets
      size <- readTVar chtSize
      load <- readTVar chtLoad
      checkHashing buckets
      checkSize buckets size
      checkLoad buckets load

    checkHashing buckets = do
      n <- getLength buckets
      for_ [0..n - 1] $ \i -> do
        bucket <- readArray buckets i
        for_ bucket $ \(k, _) -> do
          expPlace <- findPlace k buckets
          when (expPlace /= i) $
            throwM (Inconsistent "a key is in a wrong bucket")

    checkSize buckets expSize = do
      n <- getLength buckets
      sizes <- for [0..n - 1] $ \i ->
        length <$> readArray buckets i
      let actSize = sum sizes
      when (expSize /= actSize) $
        throwM (Inconsistent
               $ "actual number of elements differs from tracked size\n"
              <> "\texpected size: " <> show expSize <> "\n"
              <> "\tactual size: " <> show actSize <> "\n")

    checkLoad buckets expLoad = do
      n <- getLength buckets
      loads <- for [0..n - 1] $ \i -> do
        bucket <- readArray buckets i
        pure (if null bucket then 0 else 1)
      let actLoad = sum loads
      when (expLoad /= actLoad) $
        throwM (Inconsistent "actual load differs from the tracked one")

    getLength arr = do
      (_, end) <- getBounds arr
      pure (end + 1)
