{-# LANGUAGE DeriveAnyClass   #-}
{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE FlexibleContexts #-}

module Point
  ( Point (..)
  , Polygon
  , squaredNorm
  , norm
  , plus
  , minus
  , scalarProduct
  , crossProduct
  , perimeter
  , doubleArea
  , parPerimeter
  , parDoubleArea
  ) where

import Control.Concurrent.Async (async, wait)
import Control.DeepSeq (NFData)
import Control.Monad (replicateM)
import Data.Array.IArray (IArray, Ix, bounds, (!))
import Data.Foldable (foldl')
import Data.GenValidity (GenUnchecked, GenValid, Validity (..), check)
import Data.IORef (atomicModifyIORef', newIORef)
import GHC.Generics (Generic)

-- | Represents a point on a plane or a vector
data Point = Point
  { ptX :: {-# UNPACK #-} !Int
  , ptY :: {-# UNPACK #-} !Int
  } deriving (Eq, GenUnchecked, GenValid, Generic, NFData, Show)

instance Validity Point where
  validate (Point x y) = check (abs x <= 10000 && abs y <= 10000)
    "coordinates should be small enough, so that the squared value would not overflow"

-- | A squared norm of a vector
squaredNorm :: Point -> Int
squaredNorm (Point x y) = x * x + y * y

-- | A norm of a vector
norm :: Point -> Double
norm = sqrt . fromIntegral . squaredNorm

-- | Coordinate-wise addition of vectors
plus :: Point -> Point -> Point
plus (Point x1 y1) (Point x2 y2) = Point (x1 + x2) (y1 + y2)

-- | Coordinate-wise subtraction of vectors
minus :: Point -> Point -> Point
minus (Point x1 y1) (Point x2 y2) = Point (x1 - x2) (y1 - y2)

-- | Scalar product of two vectors
scalarProduct :: Point -> Point -> Int
scalarProduct (Point x1 y1) (Point x2 y2) = x1 * x2 + y1 * y2

-- | "Pseudo-cross product" of two vectors
--
-- Basically, a norm of a cross product of two vectors
crossProduct :: Point -> Point -> Int
crossProduct (Point x1 y1) (Point x2 y2) = x1 * y2 - x2 * y1

type Polygon = [Point]

-- | Perimeter of a polygon, points listed counter-clockwise
perimeter :: Polygon -> Double
perimeter ps = fst $ foldl' folder (0, last ps) ps
  where
    folder (acc, prev) p = (acc + norm (p `minus` prev), p)

-- | Double the area of a polygon, points listed counter-clockwise
doubleArea :: Polygon -> Int
doubleArea ps = fst $ foldl' folder (0, last ps) ps
  where
    folder (acc, prev) p = (acc + crossProduct prev p, p)

-- | Perimeter of a polygon, computed concurrently
--
-- Kinda pointless for not incredibly large polygons.
parPerimeter :: (IArray a Point, Ix i, Integral i) => a i Point -> IO Double
parPerimeter arr = do
  indexRef <- newIORef arrSt
  as <- replicateM (fromIntegral jobs) (async (worker indexRef))
  results <- traverse wait as
  pure (foldl' (+) 0 results)
  where
    (arrSt, arrEnd) = bounds arr
    batchSize = 2^(16 :: Int)
    jobs = ((arrEnd - arrSt) `div` batchSize) + 1

    worker indexRef = pureWorker
                  <$> atomicModifyIORef' indexRef (\i -> (i + batchSize, i))

    pureWorker st = fst $ foldl' folder (0, arr ! beforeSt) ps
      where
        end = min (st + batchSize) arrEnd
        beforeSt = if st == arrSt then arrEnd else st - 1
        ps = map (arr !) [st .. end]
        folder (acc, prev) p = (acc + norm (prev `minus` p), p)

-- | Double the area of a polygon, computed concurrently
--
-- Kinda pointless for not incredibly large polygons.
parDoubleArea :: (IArray a Point, Ix i, Integral i) => a i Point -> IO Int
parDoubleArea arr = do
  indexRef <- newIORef arrSt
  as <- replicateM (fromIntegral jobs) (async (worker indexRef))
  results <- traverse wait as
  pure (foldl' (+) 0 results)
  where
    (arrSt, arrEnd) = bounds arr
    batchSize = 2^(16 :: Int)
    jobs = ((arrEnd - arrSt) `div` batchSize) + 1

    worker indexRef = pureWorker
                  <$> atomicModifyIORef' indexRef (\i -> (i + batchSize, i))

    pureWorker st = fst $ foldl' folder (0, arr ! beforeSt) ps
      where
        end = min (st + batchSize) arrEnd
        beforeSt = if st == arrSt then arrEnd else st - 1
        ps = map (arr !) [st .. end]
        folder (acc, prev) p = (acc + crossProduct prev p, p)
