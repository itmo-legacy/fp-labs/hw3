module Utils (circleLikePolygon, polygonToArray) where

import Data.Array (Array, listArray)
import Data.Function ((&))

import Point (Point (..), Polygon)

circleLikePolygon :: Int -> Int -> Polygon
circleLikePolygon n r
  = [1 .. dN]
  & map (\i -> 2 * pi * i / dN)
  & map (\phi -> Point (round $ dR * cos phi) (round $ dR * sin phi))
  where
    dR = fromIntegral r
    dN = fromIntegral n :: Double

polygonToArray :: Polygon -> Array Int Point
polygonToArray polygon = listArray (1, length polygon) polygon
