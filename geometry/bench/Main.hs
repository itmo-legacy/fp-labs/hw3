module Main (main) where

import Criterion.Main (Benchmark, bench, bgroup, defaultMain, env, whnf, whnfIO)

import Point (doubleArea, parDoubleArea, parPerimeter, perimeter)
import Utils (circleLikePolygon, polygonToArray)

main :: IO ()
main = defaultMain
  [ benchPolygon (10^(4 :: Int)) "10^4"
  , benchPolygon (10^(7 :: Int)) "10^7"
  ]

benchPolygon :: Int -> String -> Benchmark
benchPolygon n groupName = env makeEnv $ \ ~(polygon, polygonArr) ->
  bgroup groupName
  [ bgroup "sequential"
    [ bench "perimeter" $ whnf perimeter polygon
    , bench "doubleArea" $ whnf doubleArea polygon
    ]
  , bgroup "parallel"
    [ bench "parPerimeter" $ whnfIO (parPerimeter polygonArr)
    , bench "parDoubleArea" $ whnfIO (parDoubleArea polygonArr)
    ]
  ]
  where
    makeEnv = let polygon = circleLikePolygon n n
                  polygonArr = polygonToArray polygon
              in pure (polygon, polygonArr)
