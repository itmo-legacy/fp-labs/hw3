module PointSpec (spec) where

import Test.Hspec (Spec, describe, it, shouldBe, specify)
import Test.QuickCheck (Property)
import Test.Validity (GenValid, forAllValid)

import Point (Point (..), crossProduct, doubleArea, minus, parDoubleArea, parPerimeter, perimeter,
              plus, scalarProduct, squaredNorm)
import Utils (polygonToArray)

spec :: Spec
spec = do
  describe "squaredNorm" $ do
    it "computes a squared norm of a vector represented by a point" $ do
      squaredNorm (Point 1 5) `shouldBe` 26
      squaredNorm (Point (-3) 4) `shouldBe` 25

  describe "plus" $ do
    it "performs coordinate-wise addition of points" $ do
      forAllValid $ \(p1, p2) ->
        p1 `plus` p2 `shouldBe` Point (ptX p1 + ptX p2) (ptY p1 + ptY p2)

    it "is commutative" $ commutative plus

  describe "minus" $ do
    it "performs coordinate-wise subtraction of points" $ do
      forAllValid $ \(p1, p2) ->
        p1 `minus` p2 `shouldBe` Point (ptX p1 - ptX p2) (ptY p1 - ptY p2)

  describe "scalarProduct" $ do
    it "calculates a scalar product of two points" $ do
      Point 1 2 `scalarProduct` Point 3 4 `shouldBe` 11

    specify "scalar product of 'a' and 'a' is the squared norm of 'a'" $ do
      forAllValid $ \a -> a `scalarProduct` a `shouldBe` squaredNorm a

    it "is commutative" $ commutative scalarProduct

  describe "crossProduct" $ do
    it "calculates a norm of a cross product of two points" $ do
      Point 1 2 `crossProduct` Point 3 4 `shouldBe` -2

    it "is anti-commutative" $ forAllValid $ \(p1, p2) ->
      p1 `crossProduct` p2 `shouldBe` -(p2 `crossProduct` p1)

  describe "perimeter" $ do
    it "calculates a perimeter of a polygon" $ do
      perimeter [] `shouldBe` 0
      perimeter polygon1 `shouldBe` 8
      perimeter polygon2 `shouldBe` 32.56922510995808

    it "is zero on single vertex polygons" $ do
      forAllValid $ \p -> perimeter [p] `shouldBe` 0

  describe "doubleArea" $ do
    it "calculates a double area of a polygon" $ do
      doubleArea [] `shouldBe` 0
      doubleArea polygon1 `shouldBe` 8
      doubleArea polygon2 `shouldBe` 42

    it "is zero on single vertex polygons" $ do
      forAllValid $ \p -> doubleArea [p] `shouldBe` 0
    it "is zero on polygons with two vertices" $ do
      forAllValid $ \(p1, p2) -> doubleArea [p1, p2] `shouldBe` 0

  describe "parPerimeter" $ do
    it "should have same results as 'perimeter'" $ do
      forAllValid $ \p -> do
        let pArr = polygonToArray p
        parArea <- parPerimeter pArr
        perimeter p `shouldBe` parArea

  describe "parDoubleArea" $ do
    it "should have same results as 'doubleArea'" $ do
      forAllValid $ \p -> do
        let pArr = polygonToArray p
        parArea <- parDoubleArea pArr
        doubleArea p `shouldBe` parArea

  where
    polygon1 = [Point 0 0, Point 2 0, Point 2 2, Point 0 2]
    polygon2 = [Point (-1) (-1), Point 5 (-3), Point 4 4, Point 3 0, Point (-6) 2]

commutative :: (GenValid a, Show a, Eq b, Show b) => (a -> a -> b) -> Property
commutative op = forAllValid $ \(a, b) -> a `op` b `shouldBe` b `op` a
