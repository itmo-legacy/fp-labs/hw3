module FSSpec (spec) where

import Lens.Micro ((^..), (^?))

import Test.Hspec (Spec, describe, it, shouldBe)

import FS (AnyFile (..), Directory (..), Regular (..), bulkReplaceExtension, cd, ls, lsRec, rmDir,
           showFile)

spec :: Spec
spec = do
  describe "cd" $ do
    it "changes current directory" $ do
      rootDir ^? cd "nested" `shouldBe` Just nestedDir
      rootDir ^? cd "nonexistent" `shouldBe` Nothing
      rootDir ^? cd "empty" `shouldBe` Just emptyDir

      rootReg ^? cd "whatever" `shouldBe` Nothing

  describe "ls" $ do
    it "lists names of all files directly residing in the current directory" $ do
      rootDir ^.. ls `shouldBe` ["foo.txt", "pass.gpg", "nested", "empty"]
      rootDir ^.. cd "nested" . ls `shouldBe` ["ungpg.sh"]
      rootReg ^.. ls `shouldBe` []

  describe "showFile" $ do
    it "gives a name of file if it exists in the cwd" $ do
      rootDir ^? showFile "foo.txt" `shouldBe` Just "foo.txt"
      rootDir ^? showFile "blabla" `shouldBe` Nothing

  describe "bulkReplaceExtension" $ do
    it "replaces extension of all regular files in a directory" $ do
      bulkReplaceExtension "7z" rootDir `shouldBe`
        mkDir "root" [mkReg "foo.7z", mkReg "pass.7z", nestedDir, emptyDir]

  describe "lsRec" $ do
    it "lists all files in a directory recursively" $ do
      lsRec rootDir `shouldBe`
        ["foo.txt","pass.gpg","nested","empty","nested/ungpg.sh"]

  describe "rmDir" $ do
    it "removes a directory if it is empty" $ do
      rmDir "nested" rootDir `shouldBe` Nothing
      rmDir "empty" rootDir `shouldBe`
        Just (mkDir "root" [mkReg "foo.txt", mkReg "pass.gpg", nestedDir])

  where
    rootDir = mkDir "root"
      [ mkReg "foo.txt", mkReg "pass.gpg", nestedDir, emptyDir ]
    nestedDir = mkDir "nested" [mkReg "ungpg.sh"]
    emptyDir = mkDir "empty" []
    rootReg = mkReg "not-a-dir.lol"

    mkDir n c = AFDir (Directory n c)
    mkReg n = AFReg (Regular n)
