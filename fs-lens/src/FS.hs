{-# LANGUAGE RankNTypes #-}

module FS
  ( AnyFile (..)
  , Directory (..)
  , Regular (..)

  , readFS
  , readAnyFile
  , cd
  , ls
  , showFile
  , bulkReplaceExtension
  , lsRec
  , rmDir
  , move
  , getPath
  ) where

import Data.Functor (($>))
import Lens.Micro (Getting, Lens', Traversal', each, failing, filtered, (%~), (&), (.~), (^.),
                   (^..), (^?), _1)
import System.Directory (doesDirectoryExist, listDirectory)
import System.FilePath (replaceExtension, takeFileName, (</>))

type FileName = String

data AnyFile = AFDir Directory | AFReg Regular deriving (Eq, Show)

data Directory = Directory
  { _directoryName     :: FileName
  , _directoryContents :: [AnyFile]
  }
  deriving (Eq, Show)

data Regular = Regular
  { _regularName     :: FileName
  }
  deriving (Eq, Show)

readFS :: FilePath -> IO Directory
readFS path = readDir path

readAnyFile :: FilePath -> IO AnyFile
readAnyFile path = do
  isDir <- doesDirectoryExist path
  if isDir then AFDir <$> (readDir path)
           else AFReg <$> (readReg path)

readDir :: FilePath -> IO Directory
readDir path = do
  let name = takeFileName path
  listing <- map (path </>) <$> listDirectory path
  contents <- traverse readAnyFile listing
  pure (Directory name contents)

readReg :: FilePath -> IO Regular
readReg path = do
  let name = takeFileName path
  pure (Regular name)

_AFDir :: Traversal' AnyFile Directory
_AFDir f (AFDir dir) = AFDir <$> f dir
_AFDir _ file'       = pure file'

_AFReg :: Traversal' AnyFile Regular
_AFReg f (AFReg dir) = AFReg <$> f dir
_AFReg _ file'       = pure file'

directoryName :: Lens' Directory FileName
directoryName f dir
  = (\new -> dir{ _directoryName = new }) <$> f (_directoryName dir)

directoryContents :: Lens' Directory [AnyFile]
directoryContents f dir
  = (\new -> dir{ _directoryContents = new }) <$> f (_directoryContents dir)

regularName :: Lens' Regular FileName
regularName f reg
  = (\new -> reg{ _regularName = new }) <$> f (_regularName reg)

dirName :: Traversal' AnyFile FileName
dirName = _AFDir . directoryName

dirContents :: Traversal' AnyFile [AnyFile]
dirContents = _AFDir . directoryContents

regName :: Traversal' AnyFile FileName
regName = _AFReg . regularName

fileName :: Lens' AnyFile FileName
fileName f (AFDir dir) = fmap AFDir (directoryName f dir)
fileName f (AFReg reg) = fmap AFReg (regularName f reg)

cd :: FileName -> Traversal' AnyFile AnyFile
cd subDirName = dirContents
              . each . filtered (\d -> d^?dirName == Just subDirName)

ls :: Traversal' AnyFile FileName
ls = dirContents . each . fileName

cr :: FileName -> Traversal' AnyFile AnyFile
cr subRegName = dirContents
              . each . filtered (\r -> r^?regName == Just subRegName)

showFile :: FileName -> Traversal' AnyFile FileName
showFile subRegName = cr subRegName . regName

bulkReplaceExtension :: String -> AnyFile -> AnyFile
bulkReplaceExtension newExt
  = dirContents . each . regName %~ (flip replaceExtension newExt)

lsRec :: AnyFile -> [FilePath]
lsRec f = f^..ls <> concatMap (lsRec' "") (f^.dirContents)
  where
    lsRec' prefix file = direct <> nested
      where
        prefix' = prefix </> file^.fileName
        direct = map (prefix' </>) (file^..ls)
        nested = concatMap (lsRec' prefix') (file^.dirContents)

rmDir :: FileName -> AnyFile -> Maybe AnyFile
rmDir dName file = case break matches (file^.dirContents) of
  (l, _ : r) -> Just (file & dirContents .~ (l ++ r))
  _          -> Nothing
  where
    matches d = d^?dirName == Just dName && null (d^.dirContents)

move :: FileName -> Getting r a (Mover a)
move name f af = mvInner <$> f (Mover name af)

getPath :: (Movable a, Monoid r) => Getting r a FilePath
getPath = moveThere . _1

data Mover a = Mover{ mvStep :: String, mvInner :: a }

class Movable a where
  moveThere :: Monoid r => Getting r a (FilePath, AnyFile)

instance Movable a => Movable (Mover a) where
  moveThere f m = maybe (pure m) ((m <$) . f) tryMove
    where
      tryMove = do
        (prevPath, prevDir) <- mvInner m ^? moveThere
        newDir <- prevDir ^? cf (mvStep m)
        pure (prevPath </> mvStep m, newDir)

      cf path = failing (cd path) (cr path)

instance Movable AnyFile where
  moveThere f af = f (af^.fileName, af) $> af
